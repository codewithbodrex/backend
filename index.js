import express from "express";
import bodyParser from "body-parser";
import productRoutes from './routes/product.js';
import categoryRoutes from './routes/category.js';
import profileRoutes from './routes/profile.js';
import cartRoutes from './routes/cart.js';

const app = express();
const PORT = 5000;


app.use(bodyParser.json());
app.use('/product' , productRoutes);
app.use('/product-category' , categoryRoutes);
app.use('/profile' , profileRoutes);
app.use('/cart' , cartRoutes);

app.get('/' , (req,res) =>res.send("Homepage"));


app.listen(PORT, () => console.log(`Server Running on http://localhost:${PORT}`));
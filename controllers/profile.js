import pool from '../database/db.js';

export const getAllProfile =  async(req,res)=>{
    try{
        const user = await pool.query("SELECT * FROM profile");
            res.json(
                {
                "status" : 200,
                "message" : `Data succesfully fetched`,
                "data" : user.rows
                }
            );            
    }catch(err){
        res.json({message:err});
    } 
}



export const login =  async(req,res)=>{
    const { name, password } = req.query;
    try{
        const foundUser = await pool.query("SELECT name from profile WHERE name = ($1) AND password = ($2)",[name, password]);
        if(foundUser.rows.length){
            res.json(
                {
                "status" : 200,
                "message" : `Login successfull with this name ${name}`,
                "data" : foundUser.rows
                }
            );            
        }else{
            res.json(
                {
                    "status" : 204,
                    "message" : `User with this name: ${name} doesn't found or your password wrong`,
                    "data" : foundUser.rows
                }

            );
        }
    }catch(err){
        res.json({message:err});
    } 
}

export const register =  async (req,res)=>{
    const {name, password} = req.body;
    try{
        const saveUser = await pool.query("INSERT INTO profile (name, password) VALUES ($1, $2) RETURNING *", [name, password]);
        res.json(
            {
                "status" : 201,
                "message" : `User with this name: ${name} succesfully inserted`,
                "data" : saveUser.rows
            }

        );
    }catch(err){
        res.json({message:err});
    }
}
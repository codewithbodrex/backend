import pool from '../database/db.js';


export const getCategoryId =  async(req,res)=>{
    const { id } = req.params;
    try{
        const foundCategory = await pool.query("SELECT * from categories WHERE id = ($1)",[id]);        
        res.json(
            {
                "status" : 200,
                "message" : `Category with this id: ${id} succesfully fetched`,
                "data" : foundCategory.rows
            }

        );
    }catch(err){
        res.json({message:err});
    } 
}

export const getCategory =  async(req,res)=>{
        try{
            const category = await pool.query("SELECT * from categories");
            res.json(
                {
                    "status" : 200,
                    "message" : `Categories succesfully fetched`,
                    "data" : category.rows
                }
    
            );
        }catch(err){
            res.json({message:err});
        } 
}
//ALTER SEQUENCE categories_id_seq RESTART WITH 1 formula for setting the increment to 1
export const createCategory =  async (req,res)=>{
    const {name} = req.body;
    try{
        const saveCategory = await pool.query("INSERT INTO categories (name) VALUES ($1) RETURNING *", [name]);
        res.json(
            {
                "status" : 200,
                "message" : `Category with this name: ${name} succesfully inserted`,
                "data" : saveCategory.rows
            }

        );
    }catch(err){
        res.json({message:err});
    }
}
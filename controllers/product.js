import pool from "../database/db.js";

export const getAllproduct = async (req,res)=>{
    try{
        const products = await pool.query("SELECT products.id, products.name, products.price, products.stock, categories.id as categories_id, categories.name as categories_name  from products INNER JOIN categories ON products.category=categories.id");


        res.json(
            {
                "status" : 200,
                "message" : "Data succesfully fetch",
                "data" : products.rows
            }

        );
    }catch(err){
        res.json({message:err});
    } 
}


export const getSingleProduct =  async (req,res)=>{
    const { id } = req.params;
    try{
        const products = await pool.query("SELECT * from products WHERE id = ($1)",[id]);

        res.json(
            {
                "status" : 200,
                "message" : `Data with this id: ${id} succesfully fetch`,
                "data" : products.rows
            }

        );

    }catch(err){
        res.json({message:err});
    }    
}

export const createProduct =  async (req,res)=>{
    const {name, price, stock, category} = req.body;
    try{
        const saveProduct = await pool.query("INSERT INTO products (name, price, stock, category) VALUES ($1, $2, $3, $4) RETURNING *", [name, price, stock, category]);

        res.json(
            {
                "status" : 201,
                "message" : "Data succesfully insert",
                "data" : saveProduct.rows
            }

        );

    }catch(err){
        res.json({message:err});
    }
}


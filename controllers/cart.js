import pool from "../database/db.js";

export const getAllCart = async (req,res)=>{
    try{
        const cart = await pool.query("SELECT * from cart");

        res.json(
            {
                "status" : 200,
                "message" : "Data succesfully fetch",
                "data" : cart.rows
            }

        );
    }catch(err){
        res.json({message:err});
    } 
}


export const getSingleCart =  async (req,res)=>{
    const { id } = req.params;
    try{
        const cart = await pool.query("SELECT * from products INNER JOIN cart ON products.id=($1)",[id]);

        res.json(
            {
                "status" : 200,
                "message" : `Data with this id: ${id} succesfully fetch`,
                "data" : cart.rows
            }

        );

    }catch(err){
        res.json({message:err});
    }    
}

export const createCart =  async (req,res)=>{
    const {products_id, total} = req.body;
    try{
        const saveCart = await pool.query("INSERT INTO cart (products_id, total) VALUES ($1, $2) RETURNING *", [products_id, total]);

        res.json(
            {
                "status" : 201,
                "message" : "Data succesfully insert",
                "data" : saveCart.rows
            }

        );

    }catch(err){
        res.json({message:err});
    }
}

export const updateCart =  async (req,res)=>{
    const { id } = req.params;
    const {products_id, total} = req.query;
    try{
        const updateCart = await pool.query("UPDATE cart SET products_id = ($1), total = ($2) WHERE id = ($3) RETURNING *", [products_id, total, id]);

        res.json(
            {
                "status" : 205,
                "message" : `Data with this id: ${id} succesfully updated`,
                "data" : updateCart.rows
            }

        );

    }catch(err){
        res.json({message:err});
    }
}

//ALTER SEQUENCE categories_id_seq RESTART WITH 1
// formula for setting the increment to 1
export const deleteCart =  async (req,res)=>{
    const { id } = req.params;
    try{
        const deleteCart = await pool.query("DELETE FROM cart WHERE id = ($1) RETURNING *", [id]);
        const updateSeq = await pool.query("ALTER SEQUENCE cart_id_seq RESTART WITH 1");
        res.json(
            {
                "status" : 200,
                "message" : `Data with this id: ${id} succesfully deleted`,
                "data" : deleteCart.rows,
            }

        );

    }catch(err){
        res.json({message:err});
    }
}
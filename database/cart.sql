CREATE TABLE cart(
    id SERIAL PRIMARY KEY,
    products_id INT,     
    total INT,
    FOREIGN KEY (products_id) REFERENCES products(id)
);
CREATE DATABASE woishop;

CREATE TABLE categories(
    id SERIAL PRIMARY KEY,
    name varchar(45) UNIQUE
);



CREATE TABLE products(
    id SERIAL PRIMARY KEY,
    name varchar(45),
    price integer,
    stock integer,
    category integer,
    FOREIGN KEY (category) REFERENCES categories(id)
);

import express from "express";
import {createProduct, getAllproduct, getSingleProduct} from "../controllers/product.js";


const router = express.Router();

router.get('/', getAllproduct);
router.post('/', createProduct);
router.get('/:id', getSingleProduct);


export default router;
import express from "express";
import {getAllProfile, login, register} from "../controllers/profile.js";

const router = express.Router();

router.get('/', getAllProfile);
router.get('/user', login);
router.post('/', register);

export default router;
import express from "express";
import {createCategory, getCategory, getCategoryId} from "../controllers/category.js";

const router = express.Router();

router.get('/:id', getCategoryId);
router.get('/', getCategory);
router.post('/', createCategory);

export default router;
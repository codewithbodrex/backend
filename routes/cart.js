import express from "express";
import { createCart, deleteCart, getAllCart, getSingleCart, updateCart } from "../controllers/cart.js";

const router = express.Router();


router.get('/', getAllCart);
router.get('/:id', getSingleCart);
router.post('/', createCart);
router.patch('/:id', updateCart);
router.delete('/:id', deleteCart);

export default router;